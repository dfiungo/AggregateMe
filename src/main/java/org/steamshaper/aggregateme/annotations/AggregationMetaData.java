package org.steamshaper.aggregateme.annotations;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.steamshaper.aggregateme.instructions.IAggregationRoot;

public class AggregationMetaData {
	@Override
	public String toString() {
		return "AggregationData [field=" + field + ", annotation=" + annotation + ", isACollection=" + isACollection
				+ ", fieldName=" + fieldName + ", setter=" + setter + ", getter=" + getter + "]";
	}

	private Field field;

	private Aggregate annotation;

	private boolean isACollection;

	private String fieldName;

	private Method setter = null;

	private Method getter = null;

	private IAggregationRoot aggregationRule;

	public AggregationMetaData(Field field) {
		this.field = field;
	}

	public Aggregate getAnnotation() {
		return annotation;
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}

	public void setAnnotation(Aggregate annotation) {
		this.annotation = annotation;
	}

	public boolean isACollection() {
		return isACollection;
	}

	public void setACollection(boolean isACollection) {
		this.isACollection = isACollection;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Method getSetter() {
		return setter;
	}

	public void setSetter(Method setter) {
		this.setter = setter;
	}

	public Method getGetter() {
		return getter;
	}

	public void setGetter(Method getter) {
		this.getter = getter;
	}

	public void setAggregationRule(IAggregationRoot compiledRule) {
		this.aggregationRule = compiledRule;
	}

	public IAggregationRoot getAggregationRule() {
		return aggregationRule;
	}

}
