package org.steamshaper.aggregateme.grammar;

import org.parboiled.BaseParser;
import org.parboiled.Rule;
import org.parboiled.annotations.BuildParseTree;
import org.parboiled.annotations.DontLabel;
import org.parboiled.annotations.Label;
import org.parboiled.annotations.MemoMismatches;
import org.parboiled.annotations.SuppressNode;
import org.parboiled.annotations.SuppressSubnodes;

/**
 * The Class EnhancedBaseGrammar.
 */
// @SuppressWarnings({ "InfiniteRecursion" })
@BuildParseTree
public abstract class EnhancedBaseGrammar extends BaseParser<Object> {

	/**
	 * Identifier.
	 * 
	 * Check if is not a java keyword start with a letter followed by an
	 * arbitrary number of lettrers or digits
	 *
	 * @return the rule
	 */
	@SuppressSubnodes
	@Label(GNames.IDENTIFIER)
	public Rule Identifier() {
		return Sequence(TestNot(Keyword()), Letter(), ZeroOrMore(LetterOrDigit()));
	}

	@SuppressSubnodes
	@Label(GNames.WRITING_IDENTIFIER)
	public Rule WritingIdentifier() {
		return Sequence(TestNot(Keyword()), Letter(), ZeroOrMore(LetterOrDigit()));
	}

	/**
	 * Comparator operator
	 *
	 * @return the rule
	 */
	@Label(GNames.COMPARISON_OPERATOR)
	public Rule ComparisonOperator() {
		return FirstOf(EQU, LT, LE, GT, GE, NOTEQUAL);
	}

	// LogicalConjunction <- '&'/'|'
	@Label(GNames.LOGICAL_CONJUNCTION)
	public Rule LogicConjunction() {
		return FirstOf(AND, OR);
	}

	/**
	 * JAVA reserved Keyword.
	 *
	 * @return the rule
	 */
	public Rule Keyword() {
		return Sequence(FirstOf("assert", "break", "case", "catch", "class", "const", "continue", "default", "do",
				"else", "enum", "extends", "finally", "final", "for", "goto", "if", "implements", "import", "interface",
				"instanceof", "new", "package", "return", "static", "super", "switch", "synchronized", "this", "throws",
				"throw", "try", "void", "while"), TestNot(LetterOrDigit()));
	}

	/**
	 * Letter.
	 *
	 * @return the rule
	 */
	Rule Letter() {
		// switch to this "reduced" character space version for a ~10% parser
		// performance speedup
		return FirstOf(CharRange('a', 'z'), CharRange('A', 'Z'), '_', '$');
		// return new JavaLetterMatcher();
	}

	/**
	 * Letter or digit.
	 *
	 * @return the rule
	 */
	@MemoMismatches
	Rule LetterOrDigit() {
		// switch to this "reduced" character space version for a ~10% parser
		// performance speedup
		return FirstOf(CharRange('a', 'z'), CharRange('A', 'Z'), CharRange('0', '9'), '_', '$');
		// return new JavaLetterOrDigitMatcher();
	}

	/**
	 * Integer literal.
	 *
	 * @return the rule
	 */
	@SuppressSubnodes
	@Label(GNames.INTEGER_NUMERIC_VALUE)
	Rule IntegerLiteral() {
		return Sequence(DecimalNumeric(), Optional(AnyOf("lL")));
	}

	/**
	 * Decimal numeral.
	 *
	 * @return the rule
	 */
	@SuppressSubnodes
	@Label(GNames.DECIMAL_NUMERIC_VALUE)
	Rule DecimalNumeric() {
		return FirstOf('0', Sequence(CharRange('1', '9'), ZeroOrMore(Digit())));
	}

	/**
	 * String literal.
	 *
	 * @return the rule
	 */
	@SuppressSubnodes
	@Label(GNames.STRING_VALUE)
	Rule StringLiteral() {
		return Sequence('\'', ZeroOrMore(FirstOf(Escape(), Sequence(TestNot(AnyOf("'\\")), ANY)).suppressSubnodes()),

				'\'');
	}

	Rule Escape() {
		return Sequence('\\', AnyOf("btnfr\"\'\\"));
	}

	@SuppressSubnodes
	Rule DecimalFloat() {
		return FirstOf(Sequence(OneOrMore(Digit()), '.', ZeroOrMore(Digit()), Optional(AnyOf("fFdD"))),
				Sequence('.', OneOrMore(Digit()), Optional(AnyOf("fFdD"))));
	}

	/**
	 * Digit.
	 *
	 * @return the rule
	 */
	Rule Digit() {
		return CharRange('0', '9');
	}
	// -------------------------------------------------------------------------
	// JLS 3.11-12 Separators, Operators
	// -------------------------------------------------------------------------

	/** The and. */
	final Rule AND = Terminal("&");

	/** The colon. */
	final Rule COLON = Terminal(":");

	/** The comma. */
	final Rule COMMA = Terminal(",");

	/** The div. */
	final Rule DIV = Terminal("/", Ch('='));

	/** The dot. */
	final Rule DOT = Terminal(".");

	/** The equ. */
	final Rule EQU = Terminal("=", Ch('='));

	/** The ge. */
	final Rule GE = Terminal(">=");

	/** The gt. */
	final Rule GT = Terminal(">", AnyOf("=>"));

	/** The lbrk. */
	final Rule LBRK = Terminal("[");

	/** The le. */
	final Rule LE = Terminal("<=");

	/** The lpar. */
	final Rule LPAR = Terminal("(");

	/** The lt. */
	final Rule LT = Terminal("<", AnyOf("=<"));

	/** The lwing. */
	final Rule LWING = Terminal("{");

	/** The minus. */
	final Rule MINUS = Terminal("-", AnyOf("=-"));

	/** The mod. */
	final Rule MOD = Terminal("%", Ch('='));

	/** The notequal. */
	final Rule NOTEQUAL = Terminal("!=");

	/** The or. */
	final Rule OR = Terminal("|");

	/** The plus. */
	final Rule PLUS = Terminal("+", AnyOf("=+"));

	/** The rbrk. */
	final Rule RBRK = Terminal("]");

	/** The rpar. */
	final Rule RPAR = Terminal(")");

	/** The rwing. */
	final Rule RWING = Terminal("}");

	/** The sr. */
	final Rule SR = Terminal(">>", AnyOf("=>"));

	/**
	 * Terminal.
	 *
	 * @param string
	 *            the string
	 * @return the rule
	 */
	@DontLabel
	@SuppressNode
	Rule Terminal(String string) {
		return Sequence(string, Spacing()).label('\'' + string + '\'');
	}

	@DontLabel
	@SuppressSubnodes
	Rule Operation(String string) {
		return Sequence(string, Spacing()).label('\'' + string + '\'');
	}

	/**
	 * Terminal.
	 *
	 * @param string
	 *            the string
	 * @param mustNotFollow
	 *            the must not follow
	 * @return the rule
	 */
	@SuppressNode
	@DontLabel
	Rule Terminal(String string, Rule mustNotFollow) {
		return Sequence(string, TestNot(mustNotFollow), Spacing()).label('\'' + string + '\'');
	}

	/**
	 * Spacing.
	 *
	 * @return the rule
	 */
	@SuppressNode
	@DontLabel
	Rule Spacing() {
		return ZeroOrMore(AnyOf(" "), EMPTY);
	}
}
