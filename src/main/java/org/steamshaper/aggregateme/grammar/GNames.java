package org.steamshaper.aggregateme.grammar;

public abstract class GNames {

	private GNames() {
	}

	public static final String OPERATION_TYPE = "OperationType";

	public static final String SUM_OPERATOR = "SumOperator";
	
	public static final String PERCENTAGE_OPERATOR = "PercentageOperator";

	public static final String IDENTIFIER = "ReadIdentifier";
	
	public static final String WRITING_IDENTIFIER = "RWIdentifier";

	public static final String PATH_IDENTIFIER = "PathIdentifier";

	public static final String COMPARISON_OPERATOR = "ComparisonOperator";

	public static final String VALUES = "Values";

	public static final String INTEGER_NUMERIC_VALUE = "IntegerNumeric";
	
	public static final String DECIMAL_NUMERIC_VALUE = "DecimalNumeric";
	
	public static final String STRING_VALUE = "StringLiteral";

	public static final String SIMPLE_CONDITION = "SimpleCondition";

	public static final String FIELD_FILTER_CONDITION = "FiledFilterCondition";

	public static final String ITEMS_SELECTOR_EXP = "ItemsSelectorExp";

	public static final String FIELD_SELECTOR = "FieldSelector";

	public static final String ON_SELECTOR = "OnSelector";

	public static final String INTO_SELECTOR = "IntoSelector";

	public static final String AGGREGATION_STATEMENT = "AggregationStatement";

	public static final String AGGREGATION_RULE = "AggregateRule";

	public static final String LOGICAL_CONJUNCTION = "LogicConjunction";


}
