package org.steamshaper.aggregateme.grammar;

import org.parboiled.Rule;
import org.parboiled.annotations.BuildParseTree;
import org.parboiled.annotations.Label;
import org.parboiled.annotations.SkipNode;

@BuildParseTree
public abstract class AggregateMeGrammar extends EnhancedBaseGrammar {

	/*-
	 *  1. Agg <- Op ('->' Prj)?
	 */
	@Label(GNames.AGGREGATION_RULE)
	public Rule AggregateRule() {
		return Sequence(Spacing(), AggregationStm(), Spacing());
	}

	@Label(GNames.AGGREGATION_STATEMENT)
	public Rule AggregationStm() {
		return OperationsType();
	}

	@Label(GNames.OPERATION_TYPE)
	@SkipNode
	public Rule OperationsType() {
		return FirstOf(Sum(), Percentage());
	}

	@Label(GNames.SUM_OPERATOR)
	public Rule Sum() {
		return Sequence(SumOperation(), Spacing(), LPAR, Spacing(), ItemsSelector(), Spacing(), RPAR, Spacing());
	}

	@SkipNode
	public Rule SumOperation() {
		return Operation("sum");
	}

	@Label(GNames.PERCENTAGE_OPERATOR)
	public Rule Percentage() {
		return Sequence(PercOperation(), Spacing(), LPAR, Spacing(), ItemsSelector(), Spacing(), On(), Spacing(),
				Into(), Spacing(), RPAR, Spacing());
	}

	final Rule ON = Terminal("ON");
	final Rule INTO = Terminal("INTO");

	@Label(GNames.ON_SELECTOR)
	public Rule On() {
		return Sequence(ON, Spacing(), FieldSelector());
	}

	@Label(GNames.INTO_SELECTOR)
	public Rule Into() {
		return Sequence(INTO, Spacing(), WritingIdentifier());
	}

	@SkipNode
	public Rule PercOperation() {
		return Operation("perc");
	}

	@Label(GNames.ITEMS_SELECTOR_EXP)
	public Rule ItemsSelector() {
		return Sequence(FieldSelector(), ZeroOrMore(Sequence(DOT, FieldSelector())));
	}

	@Label(GNames.FIELD_SELECTOR)
	public Rule FieldSelector() {
		return Sequence(Identifier(), Spacing(), OptionalFieldFilterCondition());
	}

	// FieldCondition <- '[' SimpleCondition ((LogicConjunction)
	// SimpleCondition)* ']'
	@Label(GNames.FIELD_FILTER_CONDITION)
	public Rule OptionalFieldFilterCondition() {
		return Optional(FieldFilterCondition());
	}

	@SkipNode
	public Rule FieldFilterCondition() {
		return Sequence(LBRK, Spacing(), SimpleCondition(), Spacing(),
				ZeroOrMore(Spacing(), LogicConjunction(), Spacing(), SimpleCondition()), Spacing(), RBRK);
	}

	// SimpleCondition <- Identifier ComparisonOperator ConditionValue
	@Label(GNames.SIMPLE_CONDITION)
	public Rule SimpleCondition() {
		return Sequence(PathIdentifier(), Spacing(), ComparisonOperator(), Spacing(), Values());
	}

	@Label(GNames.PATH_IDENTIFIER)
	public Rule PathIdentifier() {
		return Sequence(Identifier(), ZeroOrMore(Sequence(DOT, Identifier())));
	}

	@Label(GNames.VALUES)
	@SkipNode
	public Rule Values() {
		return FirstOf(StringLiteral(), DecimalFloat(), DecimalNumeric());
	}

}
