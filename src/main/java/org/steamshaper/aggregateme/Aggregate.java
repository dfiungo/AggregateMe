package org.steamshaper.aggregateme;

import static org.parboiled.errors.ErrorUtils.printParseErrors;
import static org.parboiled.support.ParseTreeUtils.printNodeTree;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.parboiled.Node;
import org.parboiled.Parboiled;
import org.parboiled.errors.ParseError;
import org.parboiled.parserunners.RecoveringParseRunner;
import org.parboiled.support.ParsingResult;
import org.steamshaper.aggregateme.annotations.AggregationMetaData;
import org.steamshaper.aggregateme.execution.RuleExecutor;
import org.steamshaper.aggregateme.grammar.AggregateMeGrammar;
import org.steamshaper.aggregateme.instructions.IAggregationRoot;
import org.steamshaper.aggregateme.instructions.IInstruction;
import org.steamshaper.aggregateme.instructions.InstructionFactory;

public class Aggregate {

	public final static Aggregate me = new Aggregate();

	private static final Set<String> skipSet = new HashSet<>();

	static {
		skipSet.add("ZeroOrMore");
		skipSet.add("Sequence");
	}

	private Set<Class<?>> knownTypes = Collections.newSetFromMap(new ConcurrentHashMap<Class<?>, Boolean>());

	private Set<Class<?>> knownAggregableTypes = Collections.newSetFromMap(new ConcurrentHashMap<Class<?>, Boolean>());

	private Map<Field, AggregationMetaData> ruleCache = new ConcurrentHashMap<>();

	private Map<Class<?>, List<AggregationMetaData>> idx_Class2Aggregation = new ConcurrentHashMap<>();

	public <T> T doIt(T root) {

		/*-
		 * This method compile all aggregation
		 * 
		 * 1. From the object get the actual class
		 * 2. Check if is an already known class and @AggregableBean
		 *    2(false). compileClass
		 
		 * 3. find all rule for class
		 * 4. find field type having @AggregableBean 
		 * 
		 * for 
		 *    
		 */

		// May be not initialized
		Class<? extends Object> rootBeanClass = root.getClass();

		List<AggregationMetaData> classMetadata = Collections.emptyList();
		if (Annotations.help.checkIfIsAnAggregableBean(rootBeanClass)) {
			classMetadata = getForClassFromAggregationCache(rootBeanClass, false);
			if (classMetadata == null) {
				compileFromAnnotation(rootBeanClass);

				// Now metadata should be preset,
				classMetadata = getForClassFromAggregationCache(rootBeanClass, true);
				if (classMetadata.isEmpty()) {
					System.out.println(String.format("No aggregation found for root bean %s", rootBeanClass.getName()));
					// The root bean don't expose aggregation BUT children may
					// have
					// it
				}
			}
		}



		List<Field> aggregableFields = ReflectionHelper.pls.getFieldAggregableForClass(rootBeanClass);

		for (Field aggField : aggregableFields) {

			ReflectionHelper.pls.readFieldValue(aggField, root);
			Object out = ReflectionHelper.pls.readFieldValue(aggField, root);
			if (out instanceof Collection<?>) {
				for (Object item : (Collection<?>) out) {
					item = doIt(item);
				}
			} else {
				out = doIt(out);
			}
			System.out.println(out);
			System.out.println(aggField.toGenericString());
		}
		
		for (AggregationMetaData metadata : classMetadata) {
			BigDecimal outcome = RuleExecutor.pls.execute(metadata.getAggregationRule(), root);
			try {
				System.out.println("Executing rule:" + metadata.getAnnotation().value() + " on " + root);
				metadata.getSetter().invoke(root, outcome);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return root;
	}

	public void compileFromAnnotation(Class<?> root) {

		/*-
		 * 1. read all annotated (@AggregableBean) types related to "root" bean.
		 * 2. For each type found:
		 * 		a. Search for @Aggregate annotation
		 * 		b. Check if the rule is already compiled
		 * 		c(b=yes).	continue;
		 * 		c(b=no). 	Compile and store in cache 
		 */

		// 1. PHASE
		Set<Class<?>> aggregableBeans = findAggregableBeansFromRoot(root);

		// System.err.println(aggregableBeans);

		// 2. PHASE
		// --- a STEP
		List<AggregationMetaData> idx_Rules = new ArrayList<>();

		for (Class<?> beanType : aggregableBeans) {

			idx_Rules.addAll(Annotations.help.findFieldsWithAggregation(beanType));

		}

		if (idx_Rules.size() > 0) {
			System.err.println("**************** ANNNOTATION SCAN SUMMARY *********************");
			for (AggregationMetaData mdata : idx_Rules) {
				System.err.println(String.format("- [%s] {%s} isACollection:%b", mdata.getField().toGenericString(),
						mdata.getAnnotation().value(), mdata.isACollection()));
			}
			System.err.println("***************************************************************");

		}
		// --- a STEP

		for (AggregationMetaData mdata : idx_Rules) {

			IAggregationRoot compiled = compile(mdata.getAnnotation().value());
			if (compiled == null) {
				System.err.println(String.format("- %s [%s] NOT compiled [%b]", mdata.getField().toGenericString(),
						mdata.getAnnotation().value(), compiled != null));
			} else {

				mdata.setAggregationRule(compiled);
				this.ruleCache.put(mdata.getField(), mdata);

				addToClass2AggregationCache(mdata);
				System.out.println(String.format("- %s [%s] compiled [%b]", mdata.getField(),
						mdata.getAnnotation().value(), compiled != null));
			}
		}

		if (idx_Class2Aggregation.size() > 0) {
			System.out.println("**************** CLASS 2 AGGREGATIONS IDX SUMMARY *********************");
			for (Entry<Class<?>, List<AggregationMetaData>> mdata : idx_Class2Aggregation.entrySet()) {
				System.out.println(String.format("  Class [%s]", mdata.getKey().getName()));
				for (AggregationMetaData aggregationMetaData : mdata.getValue()) {

					System.out.println(String.format("  --- %s %s Aggtegation[%s]",
							aggregationMetaData.getField().getType(), aggregationMetaData.getField().getName(),
							aggregationMetaData.getAnnotation().value()));
				}
			}
			System.out.println("***************************************************************");

		}

	}

	private void addToClass2AggregationCache(AggregationMetaData mdata) {
		synchronized (idx_Class2Aggregation) {
			List<AggregationMetaData> cacheEntry = this.idx_Class2Aggregation.get(mdata.getField().getDeclaringClass());
			if (cacheEntry == null) {
				cacheEntry = new ArrayList<>();
				idx_Class2Aggregation.put(mdata.getField().getDeclaringClass(), cacheEntry);
			}
			cacheEntry.add(mdata);
		}

	}

	private List<AggregationMetaData> getForClassFromAggregationCache(Class<?> clazz, boolean markEmptyIfNotFound) {
		synchronized (idx_Class2Aggregation) {

			List<AggregationMetaData> list = idx_Class2Aggregation.get(clazz);
			if (list != null) {
				list = Collections.unmodifiableList(list);
			} else if (markEmptyIfNotFound) {
				list = new ArrayList<AggregationMetaData>(0);
				idx_Class2Aggregation.put(clazz, list);
				list = Collections.unmodifiableList(list);
			}
			return list;

		}
	}

	private Set<Class<?>> findAggregableBeansFromRoot(Class<?> root) {
		return findAggregableBeansFromRoot(root, true);
	}

	private Set<Class<?>> findAggregableBeansFromRoot(Class<?> root, boolean recursive) {
		Set<Class<?>> output = new HashSet<>();

		// Check if this class has required annotation
		if (Annotations.help.checkIfIsAnAggregableBean(root)) {

			/*-
			 * 1. Get all fields present in this class
			 * 2. For each field
			 * 		a. Check if the field type is a Generic ( ParameterizedType)
			 * 		b(a=true).
			 * 			retrieve the actual agrs and add it to [found set]
			 * 		
			 * 		b(a=false).
			 * 			add the type to the [found set]
			 * 
			 * 
			 * 		c. filter [found set] removing not @AggregableBean and ALREADY known
			 * 
			 * 		
			 * 		d. for each item in FILTERED [found set]
			 * 			Run recursive find aggregation
			 * 			collect the recursion results
			 * 		 
			 */

			Field[] fields = root.getDeclaredFields();

			Set<Class<?>> foundSet = new HashSet<>();

			// Adding root type
			foundSet.add(root);

			for (Field field : fields) {
				Type genType = field.getGenericType();
				if (genType instanceof ParameterizedType) {
					ParameterizedType pType = (ParameterizedType) genType;
					Type[] actualTypeArguments = pType.getActualTypeArguments();
					for (Type gType : actualTypeArguments) {
						if (gType instanceof Class) {
							foundSet.add((Class<?>) gType);
						}
					}

				} else {
					foundSet.add(field.getType());
				}
			}

			foundSet = filterFoundSet(foundSet);

			if (recursive) {
				for (Class<?> innerClass : foundSet) {
					output.addAll(findAggregableBeansFromRoot(innerClass));
				}
			}

			output.addAll(foundSet);

		} // End if aggregable condition

		return output;

	}

	private Set<Class<?>> filterFoundSet(Set<Class<?>> foundSet) {

		if (foundSet.size() == 0) {
			return foundSet;
		}
		List<Class<?>> toBeRemoved = new ArrayList<>(foundSet.size());
		for (Class<?> clazz : foundSet) {
			if (knownTypes.contains(clazz)) { // Already known type from this
												// scan or other
				toBeRemoved.add(clazz);
			} else {
				knownTypes.add(clazz); // Any other scan will not re-work on
										// this type

				if (Annotations.help.checkIfIsAnAggregableBean(clazz)) {
					// This is an @AggregableBean not yet known should be leaved
					// in found set
					knownAggregableTypes.add(clazz);
				} else {
					// Not interesting bean no @AggregableBean annotation
					toBeRemoved.add(clazz);
				}
			}
		}

		// Remove phase
		foundSet.removeAll(toBeRemoved);

		return foundSet;
	}

	public IAggregationRoot compile(String rule) {
		/*
		 * 1. Generate AST 2. Navigate AST and build the rule
		 */

		Node<Object> rootNode = generateAST(rule);
		if (rootNode != null) {
			IInstruction stm = buildInstruction(rootNode, rootNode.getChildren(), rule);
			if (stm instanceof IAggregationRoot) {
				return (IAggregationRoot) stm;
			}

			throw new RuntimeException("Unexpected rule root " + stm.getClass().getSimpleName());
		}
		return null;

	}

	private IInstruction buildInstruction(Node<Object> node, List<Node<Object>> subNode, String rule) {

		String nodeType = node.getLabel();
		List<IInstruction> subStateMents = new ArrayList<>();

		// printNode(node, rule);
		List<Node<Object>> subNodeChildren = filterSkipNode(node.getChildren());

		for (Node<Object> snode : subNodeChildren) {
			subStateMents.add(buildInstruction(snode, subNodeChildren, rule));
		}

		String rawValue = rule.substring(node.getStartIndex(), node.getEndIndex());
		 System.err.println(String.format("Create: %s(%s) subStm { %s }",
		nodeType, rawValue, subStateMents));
		return InstructionFactory.get.instruction(nodeType, rawValue, subStateMents);
	}

	private List<Node<Object>> filterSkipNode(List<Node<Object>> children) {
		if (children.size() == 0) {
			return children;
		}

		List<Node<Object>> output = new ArrayList<>(children.size());

		for (Node<Object> n : children) {
			if (skipSet.contains(n.getLabel())) {
				// System.err.println("SKIP [" + n.getLabel() + "]");
				for (Node<Object> sn : n.getChildren()) {
					output.addAll(sn.getChildren());
				}

			} else {
				output.add(n);
			}
		}
		return output;
	}

	private final AggregateMeGrammar parser = Parboiled.createParser(AggregateMeGrammar.class);

	protected Node<Object> generateAST(String rule) {
		RecoveringParseRunner<Object> recoveringParseRunner = new RecoveringParseRunner<>(parser.AggregateRule());
		ParsingResult<Object> parseResult = recoveringParseRunner.run(rule);

		 System.out.println(printNodeTree(parseResult));
		if (parseResult.hasErrors()) {

			System.err.println(String.format("Unable to parse the rule %s", rule));
			for (ParseError err : parseResult.parseErrors) {
				System.err.println(String.format("- ERR: %s", printParseErrors(parseResult)));

			}
			return null;
		}

		return parseResult.parseTreeRoot;
	}

}
