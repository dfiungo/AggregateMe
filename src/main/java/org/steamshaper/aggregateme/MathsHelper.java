package org.steamshaper.aggregateme;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

public class MathsHelper {

	public static final int DEFAULT_PRECISION = 2;
	public static final RoundingMode DEFAULT_ROUNDING_MODE = RoundingMode.HALF_UP;

	MathsHelper() {
	}

	public interface GenericValueProvider<S, T extends Number> {

		public T read(S src);

		public void write(S target, T newValue);

	}

	public interface BigDecimalValueProvider<S> {

		public BigDecimal read(S src);

		public void write(S target, BigDecimal newValue);

	}

	// public <S,T extends Number> void onThisList(List<S> inputList, T
	// correctTotValue, GenericValueProvider<S, T> valueProvider){
	// //TODO
	// }

	public <S> void roundValueOnThisList(List<S> inputList, BigDecimal expectedValue,
			BigDecimalValueProvider<S> valueProvider, int precision, RoundingMode roundingMode) {
		Assert.notNull(roundingMode, "Rounding mode is MANDATORY (DEFAULT: HALF_DOWN)");
		MathContext mc = new MathContext(precision, roundingMode);
		List<BigDecimal> valueToRound = new ArrayList<>(inputList.size());
		BigDecimal maxValue = null;
		BigDecimal totValue = BigDecimal.ZERO;
		int indexMaxValue = -1;
		for (int i = 0; i < inputList.size(); i++) {
			BigDecimal tempValue = valueProvider.read(inputList.get(i)).round(mc);
			valueToRound.add(i, tempValue);
			if (maxValue == null || tempValue.compareTo(maxValue) >= 0) {
				maxValue = tempValue;
				indexMaxValue = i;
			}
			totValue = totValue.add(tempValue);
		}

		if (expectedValue.compareTo(totValue) != 0) {
			BigDecimal spurio = expectedValue.subtract(totValue);
			maxValue = maxValue.add(spurio);
			valueToRound.set(indexMaxValue, maxValue);
		}

		for (int i = 0; i < valueToRound.size(); i++) {
			valueProvider.write(inputList.get(i), valueToRound.get(i));
		}
	}

	public <S> void roundValueOnThisList(List<S> inputList, BigDecimal expectedValue,
			BigDecimalValueProvider<S> valueProvider) {
		this.roundValueOnThisList(inputList, expectedValue, valueProvider, DEFAULT_PRECISION, DEFAULT_ROUNDING_MODE);
	}
	
	private String numberToPlainString(Number number, String nullRepresentation){
		return number==null ? nullRepresentation : number.toString();
	}
	
	/**
	 * To plain string.
	 *
	 * @param number the number
	 * @param nullRepresentation the null representation
	 * @return the string
	 */
	public String toPlainString(Integer number, String nullRepresentation){
		return numberToPlainString(number, nullRepresentation);
	}
	
	/**
	 * To plain string.
	 *
	 * @param number the number
	 * @param nullRepresentation the null representation
	 * @return the string
	 */
	public String toPlainString(Long number, String nullRepresentation){
		return numberToPlainString(number, nullRepresentation);
	}
}
