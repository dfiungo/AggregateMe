package org.steamshaper.aggregateme.instructions.conditionals;

import java.util.ArrayList;
import java.util.List;

import org.steamshaper.aggregateme.instructions.AbstractInstruction;
import org.steamshaper.aggregateme.instructions.IInstruction;
import org.steamshaper.aggregateme.instructions.ILogicConjunction;

public class FieldFilterCondition extends AbstractInstruction implements IFieldFilterCondition {

	private List<ICondition> conditions = new ArrayList<>();
	private List<ILogicConjunction> logicConjunction = new ArrayList<>();

	@Override
	public void doPushSubInstructions(List<IInstruction> stms) {
		if (stms.size() > 0 && (stms.size() % 2) == 0) {
			throw new RuntimeException("Instructions should be odd [" + stms.size() + "]");
		}

		boolean expectSimpleCondition = true;
		for (IInstruction st : stms) {
			if (expectSimpleCondition) {
				this.conditions.add((ICondition) st);
				expectSimpleCondition = false;
			} else {
				this.logicConjunction.add((ILogicConjunction) st);
				expectSimpleCondition = true;
			}
		}

		if (conditions.size() == 1) { // No logic conjunction needed, create a
										// fake one to uniform matching logic
			ICondition matchNoneCondition = new ICondition() {

				@Override
				public boolean match(Object target) {
					return false;
				}
			};
			this.conditions.add(matchNoneCondition);

			ILogicConjunction leftConj = new ILogicConjunction() {

				@Override
				public boolean evaluateConjunction(boolean lhs, boolean rhs) {

					return lhs;
				}
			};

			this.logicConjunction.add(leftConj);

		}

	}

	@Override
	protected void doAfterSetup() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean matchCondition(Object value) {
		boolean tempOut = true;
		if (conditions.size() > 0) {

			tempOut = logicConjunction.get(0).evaluateConjunction(conditions.get(0).match(value),
					conditions.get(1).match(value));

			for (int i = 2; i < conditions.size(); i++) {
				tempOut = logicConjunction.get(i - 1).evaluateConjunction(tempOut, conditions.get(i).match(value));
			}
		}

		return tempOut;
	}

}
