package org.steamshaper.aggregateme.instructions.conditionals;

public interface IFieldFilterCondition {

	boolean matchCondition(Object value);

}
