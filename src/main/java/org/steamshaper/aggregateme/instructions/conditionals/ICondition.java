package org.steamshaper.aggregateme.instructions.conditionals;

public interface ICondition {
	
	
	boolean match(Object target);
}
