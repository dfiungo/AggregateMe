package org.steamshaper.aggregateme.instructions.conditionals;

import java.util.List;

import org.steamshaper.aggregateme.instructions.AbstractInstruction;
import org.steamshaper.aggregateme.instructions.IIdentifier;
import org.steamshaper.aggregateme.instructions.IInstruction;
import org.steamshaper.aggregateme.instructions.ops.IComparator;
import org.steamshaper.aggregateme.instructions.values.IValueContainer;

public class SimpleCondition extends AbstractInstruction implements ICondition {

	private IValueContainer expectedValue;
	private IComparator criteria;
	private IIdentifier field;

	@Override
	public void doPushSubInstructions(List<IInstruction> stms) {
		if (stms.size() != 3) {
			// wrong number of args
		}

		for (IInstruction st : stms) {
			if (st instanceof IValueContainer) {
				this.expectedValue = (IValueContainer) st;
			} else if (st instanceof IComparator) {
				this.criteria = (IComparator) st;
			} else if (st instanceof IIdentifier) {
				this.field = (IIdentifier) st;
			} else {
				throw new RuntimeException("Unable to handle instruction " + st.getClass().getSimpleName());
			}
		}
		if (expectedValue == null || criteria == null || field == null) {
			throw new RuntimeException("Arguments don't is not as expected");
		}

	}

	@Override
	protected void doAfterSetup() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean match(Object target) {
		
		Object fieldValue = this.field.getFieldValue(target);
		if(fieldValue == null){
			return false;
		}
		return this.criteria.compare(fieldValue, this.expectedValue.getValue(), this.expectedValue.getCompareType(fieldValue));
	}

}
