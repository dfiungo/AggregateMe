package org.steamshaper.aggregateme.instructions;

import java.util.List;

public interface IInstruction {

	void pushSubInstructions(List<IInstruction> stms);

	void setRawValue(String value);

	void afterSetup();

}
