package org.steamshaper.aggregateme.instructions.selectors;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.steamshaper.aggregateme.instructions.AbstractInstruction;
import org.steamshaper.aggregateme.instructions.IInstruction;

public class ItemsSelector extends AbstractInstruction implements IItemSelector {
	private List<FieldSelector> fieldSelectors;

	@Override
	public void doPushSubInstructions(List<IInstruction> stms) {
		fieldSelectors = new ArrayList<>(stms.size());
		for (IInstruction st : stms) {
			if (st instanceof IFieldSelector) {
				fieldSelectors.add((FieldSelector) st);
			} else {
				throw new RuntimeException("Unsupported instruction [" + st.getClass().getSimpleName() + "] expected "
						+ IFieldSelector.class.getSimpleName());
			}
		}

	}

	@Override
	protected void doAfterSetup() {

	}

	@Override
	public List<BigDecimal> select(Object rootObject) {

		List<Object> singleItemList = new ArrayList<>(1);
		singleItemList.add(rootObject);
		List<BigDecimal> output = selectItemsAsBigDecimal(rootObject);

		return output;
	}

	@Override
	public List<Object> selectObjects(Object rootObject) {
		List<Object> singleItemList = new ArrayList<>(1);
		singleItemList.add(rootObject);
		List<Object> output = selectItems(rootObject);

		return output;
	}

	private List<Object> selectItems(Object rootObject) {

		List<Object> initInput = new ArrayList<>(1);
		initInput.add(rootObject);

		List<Object> temp = recursiveSelection(0, initInput);

		

		return temp;
	}
	private List<BigDecimal> selectItemsAsBigDecimal(Object rootObject) {
		
		
		List<Object> temp = selectItems(rootObject); 
		List<BigDecimal> output =  new ArrayList<>(temp.size());
		for(Object obj : temp){
			output.add((BigDecimal) obj);
			
		}
		
		
		return output;
	}
	
	
	

	private List<Object> recursiveSelection(int filterIndex, List<Object> itemList ) {

		List<Object> nextList = this.fieldSelectors.get(filterIndex).selectObject(itemList);
		filterIndex++;
		if (filterIndex < this.fieldSelectors.size() && nextList.size() > 0) {
			return recursiveSelection(filterIndex, nextList);
		} else {
			return nextList;
		}
	}

}
