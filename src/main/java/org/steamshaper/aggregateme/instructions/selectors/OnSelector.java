package org.steamshaper.aggregateme.instructions.selectors;

import java.util.List;
import java.util.Map;

import org.steamshaper.aggregateme.instructions.AbstractInstruction;
import org.steamshaper.aggregateme.instructions.IInstruction;

public class OnSelector extends AbstractInstruction implements IFieldSelector {

	IFieldSelector fieldSelector;

	@Override
	public void doPushSubInstructions(List<IInstruction> stms) {
		for (IInstruction st : stms) {
			if (st instanceof IFieldSelector) {
				this.fieldSelector = (IFieldSelector) st;
			} else{
				throw new RuntimeException("Istruction not supported [" + st.getClass().getSimpleName() + "]");
			}

		}

	}

	@Override
	protected void doAfterSetup() {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Object> selectObject(List<Object> input) {
		return fieldSelector.selectObject(input);// output;
	}

	@Override
	public Map<Object, Object> selectSourceAndObject(List<Object> input) {
		return fieldSelector.selectSourceAndObject(input);
	}

	@Override
	public String getTargetField() {
		
		return fieldSelector.getTargetField();
	}

}
