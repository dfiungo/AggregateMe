package org.steamshaper.aggregateme.instructions.selectors;

import java.math.BigDecimal;
import java.util.List;

import org.steamshaper.aggregateme.instructions.AbstractInstruction;
import org.steamshaper.aggregateme.instructions.IIdentifier;
import org.steamshaper.aggregateme.instructions.IInstruction;
import org.steamshaper.aggregateme.instructions.IWritingIdentifier;

public class IntoSelector extends AbstractInstruction implements IIdentifier {

	IWritingIdentifier fieldIdentifier;

	@Override
	public void doPushSubInstructions(List<IInstruction> stms) {
		for (IInstruction st : stms) {
			if (st instanceof IWritingIdentifier) {
				this.fieldIdentifier = (IWritingIdentifier) st;
			} else {
				throw new RuntimeException("Istruction not supported [" + st.getClass().getSimpleName() + "]");
			}

		}

	}

	@Override
	protected void doAfterSetup() {

	}

	@Override
	public Object getFieldValue(Object bean) {
		return fieldIdentifier.getFieldValue(bean);
	}

	@Override
	public FieldType getFieldType(Object bean) {
		return fieldIdentifier.getFieldType(bean);
	}

	public void setFieldValue(Object target, BigDecimal newValue) {
		fieldIdentifier.setFieldValue(target, newValue);
		
	}

	@Override
	public String getFieldName() {
		return fieldIdentifier.getFieldName();
	}

	

}
