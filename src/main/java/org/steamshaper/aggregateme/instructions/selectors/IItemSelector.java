package org.steamshaper.aggregateme.instructions.selectors;

import java.math.BigDecimal;
import java.util.List;

public interface IItemSelector {

	List<BigDecimal> select(Object rootObject);
	
	List<Object> selectObjects(Object rootObject);

}
