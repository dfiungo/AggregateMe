package org.steamshaper.aggregateme.instructions.selectors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.steamshaper.aggregateme.instructions.AbstractInstruction;
import org.steamshaper.aggregateme.instructions.IIdentifier;
import org.steamshaper.aggregateme.instructions.IIdentifier.FieldType;
import org.steamshaper.aggregateme.instructions.conditionals.IFieldFilterCondition;
import org.steamshaper.aggregateme.instructions.IInstruction;

public class FieldSelector extends AbstractInstruction implements IFieldSelector {

	IIdentifier fieldIdentifier;
	IFieldFilterCondition fieldFilter;

	@Override
	public void doPushSubInstructions(List<IInstruction> stms) {
		for (IInstruction st : stms) {
			if (st instanceof IIdentifier) {
				this.fieldIdentifier = (IIdentifier) st;
			} else if (st instanceof IFieldFilterCondition) {
				this.fieldFilter = (IFieldFilterCondition) st;
			} else {
				throw new RuntimeException("Istruction not supported [" + st.getClass().getSimpleName() + "]");
			}

		}

	}

	@Override
	protected void doAfterSetup() {
		// TODO Auto-generated method stub

	}

	@Override

	public Map<Object, Object> selectSourceAndObject(List<Object> input) {
		Map<Object, Object> output = new HashMap<>();
		if (input.size() > 0) {
			Object bean = input.get(0);
			FieldType fType = this.fieldIdentifier.getFieldType(bean);

			switch (fType) {

			case BEAN:
			case NUMBER:
			case STRING:
				for (Object in : input) {
					Object value = this.fieldIdentifier.getFieldValue(in);
					if (this.fieldFilter.matchCondition(value)) {
						output.put(in,value);
					}
				}
				break;
			case COLLECTION:
				for (Object in : input) {
					Object value = this.fieldIdentifier.getFieldValue(in);
					@SuppressWarnings("unchecked")
					Collection<Object> values = (Collection<Object>) value;
					ArrayList<Object> outList = new ArrayList<Object>(values.size());
					output.put(in,outList);
					for (Object v : values) {
						if (this.fieldFilter.matchCondition(v)) {
							outList.add(v);
						}
					}
				}
				break;
			}

		}

		return output;
	}

	@Override
	public List<Object> selectObject(List<Object> input) {
		
		List<Object> superList =  new ArrayList<>();
		
		for( Object item :selectSourceAndObject(input).values()){
			if(item instanceof Collection<?>){
			superList.addAll((Collection<? extends Object>) item);
			}else{
				superList.add(item);
			}
		}
		
		return superList;
	}

	@Override
	public String getTargetField() {
		return fieldIdentifier.getFieldName();
	}

}
