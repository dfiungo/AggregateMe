package org.steamshaper.aggregateme.instructions.selectors;

import java.util.List;
import java.util.Map;

public interface IFieldSelector {

	List<Object> selectObject(List<Object> input);

	Map<Object,Object> selectSourceAndObject(List<Object> input);
	
	
	String getTargetField();
	
	
	
}
