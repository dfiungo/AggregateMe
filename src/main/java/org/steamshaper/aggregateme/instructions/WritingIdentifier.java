package org.steamshaper.aggregateme.instructions;

import org.steamshaper.aggregateme.ReflectionHelper;

public class WritingIdentifier extends Identifier implements IWritingIdentifier {


	@Override
	public void setFieldValue(Object target, Object newValue) {
		ReflectionHelper.pls.writeFieldValue(value, target, newValue);

	}




}
