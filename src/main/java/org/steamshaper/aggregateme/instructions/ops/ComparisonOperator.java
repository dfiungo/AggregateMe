package org.steamshaper.aggregateme.instructions.ops;

import java.math.BigDecimal;
import java.util.List;

import org.steamshaper.aggregateme.instructions.AbstractInstruction;
import org.steamshaper.aggregateme.instructions.IInstruction;

public class ComparisonOperator extends AbstractInstruction implements IComparator {

	private static final String LE_S = "<=";
	private static final String LT_S = "<";
	private static final String EQ_S = "=";
	private static final String GT_S = ">";
	private static final String GE_S = ">=";
	private static final String NE_S = "!=";

	private enum CType {
		EQU(EQ_S), LT(LT_S), LE(LE_S), GT(GT_S), GE(GE_S), NOTEQUAL(NE_S);

		private String symbol;

		CType(String symbol) {
			this.symbol = symbol;
		}

		public static CType getFromSymbol(String symbol) {
			if (LE.symbol.equals(symbol)) {
				return LE;
			} else if (LT.symbol.equals(symbol)) {
				return LT;
			} else if (EQU.symbol.equals(symbol)) {
				return EQU;
			} else if (GT.symbol.equals(symbol)) {
				return GT;
			} else if (GE.symbol.equals(symbol)) {
				return GE;
			} else if (NOTEQUAL.symbol.equals(symbol)) {
				return NOTEQUAL;
			}
			return null;
		}
	}

	private CType compareType;

	@Override
	public void doPushSubInstructions(List<IInstruction> stms) {
		if (stms.size() != 0) {
			throw new RuntimeException();
		}
		
	}

	@Override
	protected void doAfterSetup() {
		this.compareType = CType.getFromSymbol(this.getRawValue());

	}

	@Override
	public boolean compare(Object lhs, Object rhs, CompareMode compareMode) {
		int compareValue = 0;
		switch (compareMode) {
		case BIGDECIMAL:
			if (lhs instanceof BigDecimal && rhs instanceof BigDecimal) {
				BigDecimal bd_l = (BigDecimal) lhs;
				BigDecimal bd_r = (BigDecimal) rhs;

				compareValue = bd_l.compareTo(bd_r);
			}
			break;
		case NUMERIC_FLOAT:
			if (lhs instanceof Number && rhs instanceof Number) {
				double d_l = ((Number) lhs).doubleValue();
				double d_r = ((Number) rhs).doubleValue();

				compareValue = Double.compare(d_l, d_r);
			}
			break;
		case NUMERIC_INTEGER:
			if (lhs instanceof Number && rhs instanceof Number) {
				long i_l = ((Number) lhs).longValue();
				long i_r = ((Number) rhs).longValue();

				compareValue = Long.compare(i_l, i_r);
			}
			break;
		case POJO:
			if (lhs.equals(rhs)) {
				compareValue = 0;
			} else {
				compareValue = Integer.MIN_VALUE;
			}

			break;
		case STRING:
			if (lhs instanceof String && rhs instanceof String) {
				String s_l = (String) lhs;
				String s_r = (String) rhs;

				compareValue = s_l.compareTo(s_r);
			}
			break;
		default:
			break;

		}
		if (compareValue == 0 && (compareType == CType.EQU || compareType == CType.GE || compareType == CType.LE)) {
			return true;
		} else if (compareValue > 0 && (compareType == CType.GT || compareType == CType.NOTEQUAL)) {
			return true;
		} else if (compareValue < 0 && (compareType == CType.LT || compareType == CType.NOTEQUAL)) {
			return true;
		}

		return false;
	}

}
