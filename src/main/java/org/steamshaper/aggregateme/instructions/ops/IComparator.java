package org.steamshaper.aggregateme.instructions.ops;

public interface IComparator {

	public enum CompareMode{
		STRING, NUMERIC_INTEGER, NUMERIC_FLOAT, BIGDECIMAL, POJO 
	}
	boolean compare(Object lhs,Object rhs, CompareMode compareType);
	
	
	
}
