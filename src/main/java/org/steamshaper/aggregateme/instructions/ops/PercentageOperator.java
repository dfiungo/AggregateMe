package org.steamshaper.aggregateme.instructions.ops;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.steamshaper.aggregateme.ReflectionHelper;
import org.steamshaper.aggregateme.instructions.AbstractInstruction;
import org.steamshaper.aggregateme.instructions.IIdentifier.FieldType;
import org.steamshaper.aggregateme.instructions.IInstruction;
import org.steamshaper.aggregateme.instructions.selectors.IItemSelector;
import org.steamshaper.aggregateme.instructions.selectors.IntoSelector;
import org.steamshaper.aggregateme.instructions.selectors.OnSelector;

public class PercentageOperator extends AbstractInstruction implements IOperations {

	private IItemSelector selector;
	private OnSelector onSelector;
	private IntoSelector intoSelector;

	@Override
	public void doPushSubInstructions(List<IInstruction> stms) {

		if (stms.size() != 3) {
			throw new RuntimeException();
		} else {
			for (IInstruction st : stms) {
				if (st instanceof IItemSelector) {
					selector = (IItemSelector) stms.get(0);
				} else if (st instanceof OnSelector) {
					onSelector = (OnSelector) st;
				} else if (st instanceof IntoSelector) {
					intoSelector = (IntoSelector) st;
				}

			}
		}
		if (selector == null || onSelector == null || intoSelector == null)

		{
			throw new RuntimeException(String.format("Malformed rule selector:%s on:%s into:%s", (selector == null),
					(onSelector == null), (intoSelector == null)));
		}

	}

	@Override
	protected void doAfterSetup() {

	}

	private BigDecimal sum(Collection<Object> values) {
		BigDecimal sum = new BigDecimal(0);
		BigDecimal v = null;
		for (Object vO : values) {
			v = (BigDecimal) vO;
			if (v != null) {
				sum = sum.add(v);
			}
		}

		return sum;
	}

	@Override
	public BigDecimal execOn(Object rootPojo) {

		List<Object> selected = selector.selectObjects(rootPojo);

		Map<Object, Object> selectObject = onSelector.selectSourceAndObject(selected);

		BigDecimal outcome = sum(selectObject.values());
		
		BigDecimal value;
		for ( Entry<Object, Object> target : selectObject.entrySet()) {
			if (intoSelector.getFieldType(target.getKey())==FieldType.NUMBER) {
				value = (BigDecimal) ReflectionHelper.pls.readFieldValue(onSelector.getTargetField(),target.getKey());
				intoSelector.setFieldValue(target.getKey(), value.divide(outcome));
				
			}
		}

		return outcome;

	}

}
