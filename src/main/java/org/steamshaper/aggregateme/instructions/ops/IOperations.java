package org.steamshaper.aggregateme.instructions.ops;

import java.math.BigDecimal;

public interface IOperations {

	

	BigDecimal execOn(Object rootPojo);

}
