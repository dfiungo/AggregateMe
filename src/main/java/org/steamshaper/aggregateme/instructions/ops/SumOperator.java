package org.steamshaper.aggregateme.instructions.ops;

import java.math.BigDecimal;
import java.util.List;

import org.steamshaper.aggregateme.instructions.AbstractInstruction;
import org.steamshaper.aggregateme.instructions.IInstruction;
import org.steamshaper.aggregateme.instructions.selectors.IItemSelector;

public class SumOperator extends AbstractInstruction implements IOperations {

	
	private IItemSelector selector;
	
	@Override
	public void doPushSubInstructions(List<IInstruction> stms) {
		
		if(stms.size()==1 && stms.get(0) instanceof IItemSelector){
			selector = (IItemSelector) stms.get(0);
		}

		
//		System.out.println(stms);
		
		
	}

	@Override
	protected void doAfterSetup() {

	}

	
	private BigDecimal sum(List<BigDecimal> values) {
		BigDecimal sum = new BigDecimal(0);

		for (BigDecimal v : values) {
			if (v != null) {
				sum = sum.add(v);
			}
		}

		return sum;
	}

	@Override
	public BigDecimal execOn(Object rootPojo) {
		BigDecimal outcome = sum(selector.select(rootPojo));
		return outcome;
		
	}

}
