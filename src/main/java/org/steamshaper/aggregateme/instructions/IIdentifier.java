package org.steamshaper.aggregateme.instructions;

public interface IIdentifier {
	
	public enum FieldType{
		BEAN, COLLECTION, NUMBER, STRING
	}
	
	public Object getFieldValue(Object bean);

	public FieldType getFieldType(Object bean);
	
	public String getFieldName();

}
