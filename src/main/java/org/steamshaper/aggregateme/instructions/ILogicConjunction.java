package org.steamshaper.aggregateme.instructions;

public interface ILogicConjunction {
	
	
	boolean evaluateConjunction(boolean lhs,boolean rhs);

}
