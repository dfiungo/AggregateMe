package org.steamshaper.aggregateme.instructions;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.reflect.FieldUtils;

public class PathIdentifier extends AbstractInstruction implements IIdentifier {

	private String value;

	private List<IIdentifier> identifiers = new ArrayList<IIdentifier>();

	@Override
	public void doPushSubInstructions(List<IInstruction> stms) {

		for (IInstruction st : stms) {
			if (st instanceof IIdentifier) {
				identifiers.add((IIdentifier) st);
			}
		}
		System.out.println(stms);

	}

	@Override
	protected void doAfterSetup() {
		this.value = getRawValue();

	}

	@Override
	public Object getFieldValue(Object bean) {

		Object nextValue = bean;
		for (IIdentifier id : identifiers) {
			if(nextValue instanceof Collection<?>){
				throw new NotImplementedException("PathIdentifier on collection to be implemented");
			}
			if (nextValue != null) {
				nextValue = id.getFieldValue(nextValue);
			}
		}
		
		return nextValue;

	}

	private Field targetField = null;
	private FieldType fieldType = null;

	@Override
	public FieldType getFieldType(Object bean) {
		if (targetField == null) {
			targetField = readFieldInfo(bean);
		}

		if (fieldType == null) {
			fieldType = readFieldType(targetField);
		}

		return fieldType;

	}

	private FieldType readFieldType(Field field) {
		Class<?> fClass = field.getType();
		if (Collection.class.isAssignableFrom(fClass)) {
			return FieldType.COLLECTION;
		} else if (Number.class.isAssignableFrom(fClass)) {
			return FieldType.NUMBER;
		} else if (String.class.isAssignableFrom(fClass)) {
			return FieldType.STRING;
		} else {
			return FieldType.BEAN;
		}
	}

	private Field readFieldInfo(Object beanClass) {
		Field declaredField = FieldUtils.getDeclaredField(beanClass.getClass(), getRawValue(), true);
		// Class<? extends Object> fieldClass = declaredField.getType();
		if (declaredField == null) {
			throw new RuntimeException("Unable to find field [" + getRawValue() + "] in class [" + beanClass + "]"
					+ " known field is " + FieldUtils.getAllFieldsList(beanClass.getClass()));
		}
		return declaredField;
	}

	@Override
	public String getFieldName() {
		return value;
	}

}
