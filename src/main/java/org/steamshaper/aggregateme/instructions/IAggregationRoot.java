package org.steamshaper.aggregateme.instructions;

import org.steamshaper.aggregateme.instructions.ops.IOperations;

public interface IAggregationRoot {

	 IOperations getOperation();


}
