package org.steamshaper.aggregateme.instructions;

public interface IWritingIdentifier  extends IIdentifier{

	void setFieldValue(Object target, Object newValue);
	
	
}
