package org.steamshaper.aggregateme.instructions;

import java.util.List;

public abstract class  AbstractInstruction implements IInstruction {

	
	protected List<IInstruction> instructions;

	@Override
	public void pushSubInstructions(List<IInstruction> stms) {

		this.instructions = stms;
		doPushSubInstructions(this.instructions);
		
		
	}

	public abstract void doPushSubInstructions(List<IInstruction> stms);

	@Override
	public void afterSetup() {
		
		doAfterSetup();
		
		
	}


	protected abstract void doAfterSetup();


	private String rawValue;

	@Override
	public void setRawValue(String value) {
		this.rawValue = value;
	}

	
	public String getRawValue(){
		return this.rawValue;
	}


	@Override
	public String toString() {
		return this.getClass().getSimpleName() + "(rawValue=[" + rawValue + "])";
	}

	
	
	
}
