package org.steamshaper.aggregateme.instructions.values;

import java.util.List;

import org.steamshaper.aggregateme.instructions.AbstractInstruction;
import org.steamshaper.aggregateme.instructions.IInstruction;
import org.steamshaper.aggregateme.instructions.ops.IComparator.CompareMode;

public class StringLiteral extends AbstractInstruction implements IValueContainer{

	private String value;

	@Override
	public void doPushSubInstructions(List<IInstruction> stms) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void doAfterSetup() {
		this.value =  getRawValue().substring(1, getRawValue().length()-1);

	}

	@Override
	public String getStringValue() {
		return value;
	}

	@Override
	public Object getValue() {
		return getStringValue();
	}

	@Override
	public CompareMode getCompareType(Object fieldValue) {
		if(fieldValue instanceof String){
			return CompareMode.STRING;
		}
		throw new RuntimeException(String.format("Unable to compare %s - String", fieldValue.getClass().getSimpleName()));
	}


}
