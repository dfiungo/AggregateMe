package org.steamshaper.aggregateme.instructions.values;

import java.util.ArrayList;
import java.util.List;

import org.steamshaper.aggregateme.instructions.AbstractInstruction;
import org.steamshaper.aggregateme.instructions.IInstruction;

public class Values extends AbstractInstruction implements IValuesCollection{

	
	List<IValueContainer> valueContainer = new ArrayList<>(1);
	@Override
	public void doPushSubInstructions(List<IInstruction> stms) {
		for(IInstruction st : stms){
			 valueContainer.add((IValueContainer) st);
				
			
		}
	}
	@Override
	protected void doAfterSetup() {
		
	}

}
