package org.steamshaper.aggregateme.instructions.values;

import java.math.BigDecimal;

public interface INumberValueContainer extends IValueContainer {

	BigDecimal getBigDecimalValue();

	Double getFloatingPointValue();

	Long getLongValue();

}
