package org.steamshaper.aggregateme.instructions.values;

import org.steamshaper.aggregateme.instructions.ops.IComparator.CompareMode;

public interface IValueContainer {

	String getStringValue();

	Object getValue();

	CompareMode getCompareType(Object fieldValue);
}
