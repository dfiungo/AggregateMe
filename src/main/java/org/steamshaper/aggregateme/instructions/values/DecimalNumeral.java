package org.steamshaper.aggregateme.instructions.values;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

import org.steamshaper.aggregateme.instructions.AbstractInstruction;
import org.steamshaper.aggregateme.instructions.IInstruction;
import org.steamshaper.aggregateme.instructions.ops.IComparator.CompareMode;

public class DecimalNumeral extends AbstractInstruction implements INumberValueContainer {

	private BigDecimal value;
	private MathContext mc;
	
	
	public DecimalNumeral() {
		 mc =  new MathContext(10, RoundingMode.HALF_DOWN);
	}

	@Override
	public void doPushSubInstructions(List<IInstruction> stms) {
		if(stms.size()>0){
			System.err.println("Can't be");
		}

	}

	@Override
	protected void doAfterSetup() {
		this.value = new BigDecimal(this.getRawValue(),mc);
		
	}

	@Override
	public Long getLongValue() {
		return value.longValue();
	}

	@Override
	public Double getFloatingPointValue() {
		return value.doubleValue();
	}
	
	@Override
	public BigDecimal getBigDecimalValue(){
		return value;
	}

	@Override
	public String getStringValue() {
		return value.toString();
	}

	@Override
	public Object getValue() {
		return value;
	}

	@Override
	public CompareMode getCompareType(Object fieldValue) {
		if(fieldValue instanceof BigDecimal){
			return CompareMode.BIGDECIMAL;
		}
		throw new RuntimeException(String.format("Unable to compare %s - BigDecimal", fieldValue.getClass().getSimpleName()));
		
	}
	
	
	
	

}
