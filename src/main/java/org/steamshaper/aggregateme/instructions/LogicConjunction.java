package org.steamshaper.aggregateme.instructions;

import java.util.List;

public class LogicConjunction extends AbstractInstruction implements ILogicConjunction{
	
	protected enum CLType{
		AND("&"), OR("|");
		
		private String symbol;

		private CLType(String symbol) {
			this.symbol =  symbol;
		}
		
		public static CLType getFromSymbol(String symbol) {
			if (AND.symbol.equals(symbol)) {
				return AND;
			} else if (OR.symbol.equals(symbol)) {
				return OR;
			}
			return null;
		}
	}

	private CLType conjunctionType;
	

	@Override
	public void doPushSubInstructions(List<IInstruction> stms) {
		if (stms.size() != 0) {
			throw new RuntimeException();
		}

	}

	@Override
	protected void doAfterSetup() {
		this.conjunctionType = CLType.getFromSymbol(this.getRawValue().trim());
	}

	@Override
	public boolean evaluateConjunction(boolean lhs, boolean rhs) {

		switch(conjunctionType){
		case AND:
			return lhs && rhs;
		case OR:
			return lhs || rhs;
		}
		
		return false;
	}

}
