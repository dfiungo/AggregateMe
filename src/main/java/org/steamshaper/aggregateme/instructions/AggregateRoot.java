package org.steamshaper.aggregateme.instructions;

import java.util.List;

import org.steamshaper.aggregateme.instructions.ops.IOperations;

public class AggregateRoot extends AbstractInstruction implements IAggregationRoot {
	private IOperations aggregation;

	@Override
	public void doPushSubInstructions(List<IInstruction> stms) {

		if (stms.size() != 1) {
			throw new RuntimeException("Expected 1 statements received [" + stms.size() + "]");
		}

		for (IInstruction st : stms) {
			if (st instanceof IOperations) {
				this.aggregation = (IOperations) st;
			} else {
				throw new RuntimeException("Unable to handle statement [" + st.getClass().getSimpleName() + "]");
			}
		}

	}

	@Override
	protected void doAfterSetup() {
		// TODO Auto-generated method stub

	}

	@Override
	public IOperations getOperation() {
		return this.aggregation;
	}

}
