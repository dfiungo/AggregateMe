package org.steamshaper.aggregateme.instructions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.steamshaper.aggregateme.grammar.GNames;
import org.steamshaper.aggregateme.instructions.conditionals.FieldFilterCondition;
import org.steamshaper.aggregateme.instructions.conditionals.SimpleCondition;
import org.steamshaper.aggregateme.instructions.ops.ComparisonOperator;
import org.steamshaper.aggregateme.instructions.ops.PercentageOperator;
import org.steamshaper.aggregateme.instructions.ops.SumOperator;
import org.steamshaper.aggregateme.instructions.selectors.FieldSelector;
import org.steamshaper.aggregateme.instructions.selectors.IntoSelector;
import org.steamshaper.aggregateme.instructions.selectors.ItemsSelector;
import org.steamshaper.aggregateme.instructions.selectors.OnSelector;
import org.steamshaper.aggregateme.instructions.values.DecimalNumeral;
import org.steamshaper.aggregateme.instructions.values.StringLiteral;
import org.steamshaper.aggregateme.instructions.values.Values;

public class InstructionFactory {

	private Map<String, Class<? extends IInstruction>> label2class = new HashMap<>();

	public final static InstructionFactory get = new InstructionFactory();

	private InstructionFactory() {
		
		label2class.put(GNames.SUM_OPERATOR, SumOperator.class);
		
		label2class.put(GNames.PERCENTAGE_OPERATOR, PercentageOperator.class);

		label2class.put(GNames.IDENTIFIER, Identifier.class);
		
		label2class.put(GNames.WRITING_IDENTIFIER, WritingIdentifier.class);
		
		label2class.put(GNames.PATH_IDENTIFIER, PathIdentifier.class);
		
		label2class.put(GNames.COMPARISON_OPERATOR, ComparisonOperator.class);

		label2class.put(GNames.VALUES, Values.class);
		
		label2class.put(GNames.DECIMAL_NUMERIC_VALUE, DecimalNumeral.class);
		
		label2class.put(GNames.STRING_VALUE, StringLiteral.class);

		label2class.put(GNames.SIMPLE_CONDITION, SimpleCondition.class);

		label2class.put(GNames.FIELD_FILTER_CONDITION, FieldFilterCondition.class);
	
		label2class.put(GNames.ITEMS_SELECTOR_EXP, ItemsSelector.class);

		label2class.put(GNames.FIELD_SELECTOR, FieldSelector.class);
		
		label2class.put(GNames.ON_SELECTOR, OnSelector.class);

		label2class.put(GNames.INTO_SELECTOR, IntoSelector.class);
		
		label2class.put(GNames.AGGREGATION_RULE,AggregateRoot.class);

		label2class.put(GNames.LOGICAL_CONJUNCTION, LogicConjunction.class);
		
		

	}

	public IInstruction instruction(String instructionName, String value, List<IInstruction> ists) {

		Class<? extends IInstruction> istClass = label2class.get(instructionName);

		if (istClass == null) {
			System.err.println("--------------------------------------------------");
			System.err.println("Unknown Instruction [" + instructionName + "]");
			System.err.println("--------------------------------------------------");
		}
		IInstruction newIst = null;
		try {
			newIst = istClass.newInstance();

			newIst.pushSubInstructions(ists);

			newIst.setRawValue(value);
			
			newIst.afterSetup();

		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return newIst;
	}

}
