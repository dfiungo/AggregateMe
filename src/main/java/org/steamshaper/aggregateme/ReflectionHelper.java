package org.steamshaper.aggregateme;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.steamshaper.aggregateme.annotations.AggregableBean;

public class ReflectionHelper {

	private ReflectionHelper() {

	}

	public static final ReflectionHelper pls = new ReflectionHelper();

	private Map<Class<?>, List<Field>> idx_class2Field = new ConcurrentHashMap<>();

	public List<Field> getFieldForClass(Class<?> clazz) {

		List<Field> output = idx_class2Field.get(clazz);

		if (output == null) {

			Field[] declaredFields = clazz.getDeclaredFields();
			output = new ArrayList<>(Arrays.asList(declaredFields));
		}

		return Collections.unmodifiableList(output);

	}

	private Map<Class<?>, List<Field>> idx_class2AggregableField = new ConcurrentHashMap<>();

	public List<Field> getFieldAggregableForClass(Class<?> clazz) {

		List<Field> allField = this.getFieldForClass(clazz);

		List<Field> output = idx_class2AggregableField.get(clazz);
		if (output == null) {
			output = new ArrayList<>(allField.size());
			idx_class2AggregableField.put(clazz, output);

			for (Field field : allField) {
				Type genType = field.getGenericType();
				if (genType instanceof ParameterizedType) {
					ParameterizedType pType = (ParameterizedType) genType;
					Type[] actualTypeArguments = pType.getActualTypeArguments();
					for (Type gType : actualTypeArguments) {
						if (gType instanceof Class && isAggregableBeanAnnotated(gType)) {
							output.add(field);
						}
					}

				} else if (isAggregableBeanAnnotated(field.getType())) {
					output.add(field);
				}
			}

		}

		return Collections.unmodifiableList(output);

	}

	private boolean isAggregableBeanAnnotated(Type gType) {
		return isAggregableBeanAnnotated((Class<?>) gType);
	}

	private boolean isAggregableBeanAnnotated(Class<?> gType) {
		return gType.isAnnotationPresent(AggregableBean.class);
	}

	public Object readFieldValue(Field field, Object root) {
		Object output = null;
		try {
			output = findGetter(field).invoke(root);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException(e.getMessage());
		}
		return output;

	}

	public Object readFieldValue(String fieldName, Object root) {

		try {
			return readFieldValue(root.getClass().getDeclaredField(fieldName), root);
		} catch (NoSuchFieldException | SecurityException e) {
			throw new RuntimeException(e.getMessage());
		}

	}

	public void writeFieldValue(String fieldName, Object root, Object newValue) {
		try {
			findSetter(root.getClass().getDeclaredField(fieldName)).invoke(root, newValue);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchFieldException
				| SecurityException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	public Method findSetter(Field field) {
		try {
			return field.getDeclaringClass().getMethod("set" + StringUtils.capitalize(field.getName()),
					field.getType());
		} catch (NoSuchMethodException | SecurityException e) {
		}
		return null;
	}

	private Map<Field, Method> idx_field2getter = new ConcurrentHashMap<>();

	public Method findGetter(Field field) {

		Method getter = idx_field2getter.get(field);
		if (getter == null) {
			try {
				getter = field.getDeclaringClass().getMethod("get" + StringUtils.capitalize(field.getName()));
				idx_field2getter.put(field, getter);
			} catch (NoSuchMethodException | SecurityException e) {
			}
		}

		return getter;
	}
}
