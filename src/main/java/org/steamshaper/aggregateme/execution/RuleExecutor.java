package org.steamshaper.aggregateme.execution;

import java.math.BigDecimal;

import org.steamshaper.aggregateme.instructions.IAggregationRoot;

public class RuleExecutor {
	
	
	
	private RuleExecutor(){
		
	}
	
	public static final RuleExecutor pls =  new RuleExecutor();
	
	public BigDecimal execute(IAggregationRoot rule,Object rootPojo){
		
		return rule.getOperation().execOn(rootPojo);
		
	}

}
