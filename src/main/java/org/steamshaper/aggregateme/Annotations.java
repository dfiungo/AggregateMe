package org.steamshaper.aggregateme;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.steamshaper.aggregateme.annotations.AggregableBean;
import org.steamshaper.aggregateme.annotations.Aggregate;
import org.steamshaper.aggregateme.annotations.AggregationMetaData;

public class Annotations {

	public static final Annotations help = new Annotations();

	public List<AggregationMetaData> findFieldsWithAggregation(Class<?> target) {
		List<AggregationMetaData> aggregableFieldsMeta = new ArrayList<>();

		for (Field field : target.getDeclaredFields()) {
			Aggregate aggAnnotation = field.getAnnotation(Aggregate.class);
			if (aggAnnotation != null) {
				AggregationMetaData metaData = new AggregationMetaData(field);

				metaData.setGetter(ReflectionHelper.pls.findGetter(field));
				metaData.setSetter(ReflectionHelper.pls.findSetter(field));
				metaData.setAnnotation(aggAnnotation);
				metaData.setACollection(Collection.class.isAssignableFrom(field.getType()));

				if (metaData.isACollection() && metaData.getGetter() == null) {
					System.err.println(String.format("ERROR: FIX me GETTER is required to access collection in class [%s] field [%s]",
							target.getName(),field.toGenericString()));
				} else if (metaData.getSetter() == null) {
					System.err.println(String.format("ERROR: FIX me SETTER is required to access field in class [%s] field [%s]",
							target.getName(),field.toGenericString()));
				}

				metaData.setFieldName(field.getName());

				aggregableFieldsMeta.add(metaData);
			}

		}
		return aggregableFieldsMeta;
	}

	public boolean checkIfIsAnAggregableBean(Class<?> target) {
		return target.isAnnotationPresent(AggregableBean.class);

	}

	

}
