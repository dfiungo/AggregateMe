package org.steamshaper.aggregateme.instructions;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;
import org.steamshaper.aggregateme.test.bean.GrandfatherBean;

public class IdentifierTest {

	@Test
	public void test() {
		Identifier tested = new Identifier();
		tested.setRawValue("age");
		tested.afterSetup();
		
		
		GrandfatherBean gb = new GrandfatherBean();
		gb.setAge(BigDecimal.TEN);
		assertEquals(BigDecimal.TEN,tested.getFieldValue(gb));
	}

}
