package org.steamshaper.aggregateme;

import static org.junit.Assert.*;

import org.junit.Test;
import org.steamshaper.aggregateme.test.bean.GrandfatherBean;

public class AnnotationsTest {

//	@Test
//	public void testFindFieldsWithAggregationObject() {
//		Map<String, AggregationData> test = Annotations.help.findFieldsWithAggregation(new GrandfatherBean());
//		System.err.println(test);
//	}

//	@Test
//	public void testFindFieldsWithAggregationClassOfQ() {
//		Map<String, AggregationData> test = Annotations.help.findFieldsWithAggregation(GrandfatherBean.class);
//		System.err.println(test);
//	}
//
//	@Test
//	public void testCheckIfIsAnAggregableBeanObject() {
//		assertTrue(Annotations.help.checkIfIsAnAggregableBean(new GrandfatherBean()));
//	}

	@Test
	public void testCheckIfIsAnAggregableBeanClassOfQ() {
		assertTrue(Annotations.help.checkIfIsAnAggregableBean(GrandfatherBean.class));
	}

}
