package org.steamshaper.aggregateme;

import static org.junit.Assert.fail;

import static org.parboiled.errors.ErrorUtils.printParseErrors;
import static org.parboiled.support.ParseTreeUtils.printNodeTree;
import static org.parboiled.trees.GraphUtils.printTree;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.parboiled.Parboiled;
import org.parboiled.parserunners.RecoveringParseRunner;
import org.parboiled.support.ParsingResult;
import org.steamshaper.aggregateme.grammar.AggregateMeGrammar;

public class AggregateMeGrammarTest {

	AggregateMeGrammar parser;

	@Before
	public void setUp() throws Exception {
		parser = Parboiled.createParser(AggregateMeGrammar.class);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testAggregateRule() {
		RecoveringParseRunner recoveringParseRunner = new RecoveringParseRunner(parser.AggregateRule());

		String inputDecimal = "sum(one[test = 10].two)";
		ParsingResult resultDecimal = recoveringParseRunner.run(inputDecimal);
		String inputString = "sum(one[test = 10].two)";
		ParsingResult resultString = recoveringParseRunner.run(inputString);
		String inputFloat = "perc(one[test = 10] ON two INTO perc)";
		ParsingResult resultFloat = recoveringParseRunner.run(inputFloat);

		System.err.println(printNodeTree(resultString));
		
		System.err.println(printParseErrors(resultString));

		Assert.assertFalse(resultDecimal.hasErrors());
		Assert.assertFalse(resultString.hasErrors());
		Assert.assertFalse(resultFloat.hasErrors());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testOp() {
		RecoveringParseRunner recoveringParseRunner = new RecoveringParseRunner(parser.AggregationStm());

		String inputDecimal = "sum(one[test = 10].two)";
		ParsingResult resultDecimal = recoveringParseRunner.run(inputDecimal);
		String inputString = "sum(one[test = 10].two)";
		ParsingResult resultString = recoveringParseRunner.run(inputString);
		String inputFloat = "perc(one[test = 10] ON two INTO perc)";
		ParsingResult resultFloat = recoveringParseRunner.run(inputFloat);

		System.err.println(printNodeTree(resultString));
		System.err.println(printParseErrors(resultString));
		System.err.println(resultString.valueStack);
		
		 Assert.assertFalse(resultDecimal.hasErrors());
		Assert.assertFalse(resultString.hasErrors());
		 Assert.assertFalse(resultFloat.hasErrors());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testOperationsType() {
		RecoveringParseRunner recoveringParseRunner = new RecoveringParseRunner(parser.OperationsType());
		String inputSUM = "sum(ciao)";
		ParsingResult resultSUM = recoveringParseRunner.run(inputSUM);
//		String inputPERC = "perc";
//		ParsingResult resultPERC = recoveringParseRunner.run(inputPERC);
//
		System.err.println(printNodeTree(resultSUM));
		System.err.println(printParseErrors(resultSUM));

		Assert.assertFalse(resultSUM.hasErrors());
	}

	// @Test TBC
	public void testProjection() {
		fail("Not yet implemented");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testItemSelector() {
		RecoveringParseRunner recoveringParseRunner = new RecoveringParseRunner(parser.ItemsSelector());

		String inputDecimal = "one.two";
		ParsingResult resultDecimal = recoveringParseRunner.run(inputDecimal);
		String inputString = "one[test = 'ciao' & test2 = 10].two";
		ParsingResult resultString = recoveringParseRunner.run(inputString);
		String inputFloat = "one[test = 'ciao' & test2 = 10 | test = 1.23 ].two.tree";
		ParsingResult resultFloat = recoveringParseRunner.run(inputFloat);

		System.err.println(printNodeTree(resultString));
		System.err.println(printParseErrors(resultDecimal));

		Assert.assertFalse(resultDecimal.hasErrors());
		Assert.assertFalse(resultString.hasErrors());
		Assert.assertFalse(resultFloat.hasErrors());

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testFieldSelector() {

		RecoveringParseRunner recoveringParseRunner = new RecoveringParseRunner(parser.FieldSelector());

		String inputSimple = "one";
		ParsingResult resultDecimal = recoveringParseRunner.run(inputSimple);
		String inputString = "one[test = 'ciao' & test2 = 10]";
		ParsingResult resultString = recoveringParseRunner.run(inputString);
		String inputFloat = "one[test = 'ciao' & test2 = 10 | test = 1.23 ]";
		ParsingResult resultFloat = recoveringParseRunner.run(inputFloat);

		// System.err.println(printNodeTree(resultString));
		System.err.println(printParseErrors(resultDecimal));

		Assert.assertFalse(resultDecimal.hasErrors());
		Assert.assertFalse(resultString.hasErrors());
		Assert.assertFalse(resultFloat.hasErrors());

	}

	@SuppressWarnings({ "rawtypes" })
	@Test
	public void testFieldFilterCondition() {
		RecoveringParseRunner recoveringParseRunner = new RecoveringParseRunner(parser.FieldFilterCondition());

		String inputDecimal = "[test = 10]";
		ParsingResult resultDecimal = recoveringParseRunner.run(inputDecimal);
		String inputString = "[test = 'ciao' & test2 = 10]";
		ParsingResult resultString = recoveringParseRunner.run(inputString);
		String inputFloat = "[test = 'ciao' & test2 = 10 | test = 1.23 ]";
		ParsingResult resultFloat = recoveringParseRunner.run(inputFloat);

		// System.err.println(printNodeTree(resultString));
		System.err.println(printParseErrors(resultDecimal));

		Assert.assertFalse(resultDecimal.hasErrors());
		Assert.assertFalse(resultString.hasErrors());
		Assert.assertFalse(resultFloat.hasErrors());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testSimpleCondition() {
		RecoveringParseRunner recoveringParseRunner = new RecoveringParseRunner(parser.SimpleCondition());

		String inputDecimal = "test = 10";
		ParsingResult resultDecimal = recoveringParseRunner.run(inputDecimal);
		String inputString = "test = 'ciao'";
		ParsingResult resultString = recoveringParseRunner.run(inputString);
		String inputFloat = "test = 1.23";
		ParsingResult resultFloat = recoveringParseRunner.run(inputFloat);

		 System.err.println(printNodeTree(resultString));
		System.err.println(printParseErrors(resultDecimal));

		Assert.assertFalse(resultDecimal.hasErrors());
		Assert.assertFalse(resultString.hasErrors());
		Assert.assertFalse(resultFloat.hasErrors());

	}
	
	
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testFailingAggregateRule() {
		RecoveringParseRunner recoveringParseRunner = new RecoveringParseRunner(parser.AggregateRule());

		String input = "sum(one[test.nested = 10].two)";
		ParsingResult result = recoveringParseRunner.run(input);
	
		System.err.println(printNodeTree(result));
		
		System.err.println(printParseErrors(result));

		Assert.assertFalse(result.hasErrors());
	}


}
