package org.steamshaper.aggregateme.test.bean;

import java.math.BigDecimal;

public class FriendBean {

	public BigDecimal getAge() {
		return age;
	}

	public void setAge(BigDecimal age) {
		this.age = age;
	}

	private BigDecimal age;

	@Override
	public String toString() {
		return "FriendBean [age=" + age + "]";
	}
	
	
}
