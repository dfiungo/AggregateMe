package org.steamshaper.aggregateme.test.bean;

import java.math.BigDecimal;
import java.util.List;

import org.steamshaper.aggregateme.annotations.AggregableBean;
import org.steamshaper.aggregateme.annotations.Aggregate;

@AggregableBean
public class FatherBean {


	
	private List<SonBean> sons;
	
	
	
	private BigDecimal age;
	
	
	@Aggregate("sum(sons.age)")
	private BigDecimal sumAllSonAge;
	
	@Aggregate("sum(sons[sex='male'].age)")
	private BigDecimal sumMaleSonAge;
	
	

	public List<SonBean> getSons() {
		return sons;
	}


	public void setSons(List<SonBean> sons) {
		this.sons = sons;
	}


	public BigDecimal getAge() {
		return age;
	}


	public void setAge(BigDecimal age) {
		this.age = age;
	}


	public BigDecimal getSumAllSonAge() {
		return sumAllSonAge;
	}


	public void setSumAllSonAge(BigDecimal sumAllSonAge) {
		this.sumAllSonAge = sumAllSonAge;
	}


	@Override
	public String toString() {
		return "FatherBean [sons=" + sons + ", age=" + age + ", sumAllSonAge=" + sumAllSonAge + ", sumMaleSonAge="
				+ getSumMaleSonAge() + "]";
	}


	public BigDecimal getSumMaleSonAge() {
		return sumMaleSonAge;
	}


	public void setSumMaleSonAge(BigDecimal sumMaleSonAge) {
		this.sumMaleSonAge = sumMaleSonAge;
	}
}
