package org.steamshaper.aggregateme.test.bean;

import java.math.BigDecimal;

import org.steamshaper.aggregateme.annotations.AggregableBean;
import org.steamshaper.aggregateme.annotations.Aggregate;

@AggregableBean
public class GrandfatherBean {
	
	private FatherBean father;
	
	
	private BigDecimal age;
	
	
	
	@Aggregate("perc(father.sons ON money INTO percGrandSon) ")
	private BigDecimal  grandsonPocketMoney;

	
	@Aggregate("sum(father.sons.age)")	
	private BigDecimal sonsAge;
	
	public FatherBean getFather() {
		return father;
	}
	
	public void setFather(FatherBean father) {
		this.father = father;
	}
	
	public BigDecimal getAge() {
		return age;
	}
	
	public void setAge(BigDecimal age) {
		this.age = age;
	}

	public BigDecimal getSonsAge() {
		return sonsAge;
	}

	public void setSonsAge(BigDecimal sonsAge) {
		this.sonsAge = sonsAge;
	}

	public BigDecimal getGrandsonPocketMoney() {
		return grandsonPocketMoney;
	}

	public void setGrandsonPocketMoney(BigDecimal grandsonPocketMoney) {
		this.grandsonPocketMoney = grandsonPocketMoney;
	}

	

}
