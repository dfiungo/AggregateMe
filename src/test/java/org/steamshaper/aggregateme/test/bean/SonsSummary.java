package org.steamshaper.aggregateme.test.bean;

import java.math.BigDecimal;

public class SonsSummary {

	private BigDecimal totalSonsAge;

	public BigDecimal getTotalSonsAge() {
		return totalSonsAge;
	}

	public void setTotalSonsAge(BigDecimal totalSonsAge) {
		this.totalSonsAge = totalSonsAge;
	}
	
	
}
