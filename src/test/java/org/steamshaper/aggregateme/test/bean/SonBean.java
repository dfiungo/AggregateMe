package org.steamshaper.aggregateme.test.bean;

import java.math.BigDecimal;
import java.util.List;

import org.steamshaper.aggregateme.annotations.AggregableBean;

@AggregableBean
public class SonBean {



	@Override
	public String toString() {
		return "SonBean [grandsons=" + grandsons + ", friends=" + friends + ", bestFriend=" + bestFriend + ", age="
				+ age + ", name=" + name + ", sex=" + sex + ", money=" + money + ", percGrandSon=" + percGrandSon + "]";
	}


	private List<GrandsonBean> grandsons;

	private List<FriendBean> friends;

	private FriendBean bestFriend;

	private BigDecimal age;

	private String name;
	
	private String sex;

	private BigDecimal money;
	
	private BigDecimal percGrandSon;


	public List<GrandsonBean> getGrandsons() {
		return grandsons;
	}


	public void setGrandsons(List<GrandsonBean> grandsons) {
		this.grandsons = grandsons;
	}


	public List<FriendBean> getFriends() {
		return friends;
	}


	public void setFriends(List<FriendBean> friends) {
		this.friends = friends;
	}


	public FriendBean getBestFriend() {
		return bestFriend;
	}


	public void setBestFriend(FriendBean bestFriend) {
		this.bestFriend = bestFriend;
	}


	public BigDecimal getAge() {
		return age;
	}


	public void setAge(BigDecimal age) {
		this.age = age;
	}


	public void setName(String name) {
		this.name = name;
		
	}
	
	public String getName(){
		return this.name;
	}


	public String getSex() {
		return sex;
	}


	public void setSex(String sex) {
		this.sex = sex;
	}


	public BigDecimal getMoney() {
		return money;
	}


	public void setMoney(BigDecimal money) {
		this.money = money;
	}


	public BigDecimal getPercGrandSon() {
		return percGrandSon;
	}


	public void setPercGrandSon(BigDecimal percGrandSon) {
		this.percGrandSon = percGrandSon;
	}
	
}
