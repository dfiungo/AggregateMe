package org.steamshaper.aggregateme;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.junit.Test;
import org.steamshaper.aggregateme.execution.RuleExecutor;
import org.steamshaper.aggregateme.instructions.IAggregationRoot;
import org.steamshaper.aggregateme.test.bean.FatherBean;
import org.steamshaper.aggregateme.test.bean.FriendBean;
import org.steamshaper.aggregateme.test.bean.GrandfatherBean;
import org.steamshaper.aggregateme.test.bean.SonBean;


import static org.parboiled.errors.ErrorUtils.printParseErrors;
import static org.parboiled.support.ParseTreeUtils.printNodeTree;
import static org.junit.Assert.*;

public class AggregateMePreFlightTest {

	@Test
	public void test() {
		// AggregateMeGrammar parser =
		// Parboiled.createParser(AggregateMeGrammar.class);
		// RecoveringParseRunner<Node> recoveringParseRunner = new
		// RecoveringParseRunner<>(parser.AggregateRule());

		String inputRule = "sum(one[test = 10 & ciao='ciao'].two)".replace(" ", "");

		IAggregationRoot output = Aggregate.me.compile(inputRule);
		System.out.println(output);
	}

	@Test
	public void testExecutionSimple() {

		GrandfatherBean gfb = new GrandfatherBean();
		gfb.setAge(new BigDecimal(90));
		FatherBean fat = new FatherBean();
		gfb.setFather(fat);
		fat.setAge(new BigDecimal(70));
		ArrayList<SonBean> sons = new ArrayList<SonBean>();
		fat.setSons(sons);
		SonBean s1 = new SonBean();
		sons.add(s1);
		s1.setAge(new BigDecimal(50));

		SonBean s2 = new SonBean();
		sons.add(s2);
		s2.setAge(new BigDecimal(49));

		String inputRule = "sum(father.sons.age)";

		IAggregationRoot output = Aggregate.me.compile(inputRule);

		BigDecimal executeRes = RuleExecutor.pls.execute(output, gfb);
		assertTrue("Value expected 99 read " + executeRes, executeRes.compareTo(new BigDecimal(99)) == 0);

		System.out.println(output);
	}

	@Test
	public void testExecutionCondition() {

		GrandfatherBean gfb = new GrandfatherBean();
		gfb.setAge(new BigDecimal(90));
		FatherBean fat = new FatherBean();
		gfb.setFather(fat);
		fat.setAge(new BigDecimal(70));
		ArrayList<SonBean> sons = new ArrayList<SonBean>();
		fat.setSons(sons);
		SonBean s1 = new SonBean();
		s1.setName("Alice");
		sons.add(s1);
		s1.setAge(new BigDecimal(50));

		SonBean s2 = new SonBean();
		sons.add(s2);
		s2.setAge(new BigDecimal(49));

		String inputRule = "sum(father.sons[name='Alice' | name='Bob'].age)";

		IAggregationRoot output = Aggregate.me.compile(inputRule);

		BigDecimal executeRes = RuleExecutor.pls.execute(output, gfb);
		assertTrue("Value expected 50 read " + executeRes, executeRes.compareTo(new BigDecimal(50)) == 0);

		System.out.println(output);
	}
	
	
	@Test
	public void testExecutionPathCondition() {

		GrandfatherBean gfb = new GrandfatherBean();
		gfb.setAge(new BigDecimal(90));
		FatherBean fat = new FatherBean();
		gfb.setFather(fat);
		fat.setAge(new BigDecimal(70));
		ArrayList<SonBean> sons = new ArrayList<SonBean>();
		fat.setSons(sons);
		SonBean s1 = new SonBean();
		s1.setName("Alice");
		FriendBean bestFriend = new FriendBean();
		bestFriend.setAge(new BigDecimal(45));
		s1.setBestFriend(bestFriend);
		sons.add(s1);
		s1.setAge(new BigDecimal(50));

		SonBean s2 = new SonBean();
		sons.add(s2);
		s2.setAge(new BigDecimal(49));

		String inputRule = "sum(father.sons[name='Alice' & bestFriend.age=45].age)".replace(" ", "");

		IAggregationRoot output = Aggregate.me.compile(inputRule);

		BigDecimal executeRes = RuleExecutor.pls.execute(output, gfb);
		assertTrue("Value expected 50 read " + executeRes, executeRes.compareTo(new BigDecimal(50)) == 0);

		System.out.println(output);
	}
	
	

	@Test
	public void testCompileFromClass() {
		Aggregate.me.compileFromAnnotation(GrandfatherBean.class);
	}

	@Test
	public void testDoIT() {
		
		GrandfatherBean gfb = new GrandfatherBean();
		gfb.setAge(new BigDecimal(90));
		FatherBean fat = new FatherBean();
		gfb.setFather(fat);
		fat.setAge(new BigDecimal(70));
		ArrayList<SonBean> sons = new ArrayList<SonBean>();
		fat.setSons(sons);
		SonBean s1 = new SonBean();
		s1.setName("Alice");
		sons.add(s1);
		s1.setAge(new BigDecimal(50));
		s1.setMoney(BigDecimal.TEN);

		SonBean s2 = new SonBean();
		s2.setMoney(BigDecimal.TEN);
		s2.setSex("male");
		sons.add(s2);
		s2.setAge(new BigDecimal(49));

		
		assertTrue(gfb.getSonsAge()==null);
		assertTrue(gfb.getFather().getSumMaleSonAge()==null);
		gfb = Aggregate.me.doIt(gfb);
		assertTrue("Value expected 99 read " + gfb.getSonsAge(), gfb.getSonsAge().compareTo(new BigDecimal(99)) == 0);
		assertTrue("Value expected 99 read " + gfb.getFather().getSumMaleSonAge(), gfb.getFather().getSumMaleSonAge().compareTo(new BigDecimal(49)) == 0);
		assertTrue("Value expected 99 read " + gfb.getSonsAge(), gfb.getGrandsonPocketMoney().compareTo(new BigDecimal(20)) == 0);
		assertTrue("Value expected 99 read " + gfb.getSonsAge(), gfb.getFather().getSons().get(0).getPercGrandSon().compareTo(new BigDecimal(0.5)) == 0);
		assertTrue("Value expected 99 read " + gfb.getSonsAge(), gfb.getFather().getSons().get(1).getPercGrandSon().compareTo(new BigDecimal(0.5)) == 0);
		
	}

}
