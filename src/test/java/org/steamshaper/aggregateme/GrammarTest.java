package org.steamshaper.aggregateme;

/*-
 * The Class GrammarTest.
 *	
 *	With projection
 *  1. Agg		<- Op ('->' Prj)?
 *  2. Op		<- Type  Arg
 *  3. Type		<- 'sum' | 'perc' | 'mult'
 *  4. Arg		<- '(' Path ')'
 *  5. Path		<- [a-z]+ '.' Path | [a-z]+
 * 	6. Prj		<- [a-z]+ '.' Path | [a-z]+
 * 
 * 	With single filter
 *  1f. Agg		<- Op ('->' Prj)?
 *  2f. Op		<- Type / Arg
 *  3f. Type		<- 'sum' | 'perc' | 'mult'
 *  4f. Arg		<- '(' Path ')'
 *  5f. Path		<- [a-z]+ Filter? '.' Path | [a-z]+
 *  6f. Filter	<- '[' [anyJavaFieldLegalChar]+ '=' [anyCharacter]+ ']'
 * 
 * 	With complex filter
 * 	1. Agg		<- Op ('->' Prj)?
 *  2. Op		<- Type / Arg
 *  3. Type		<- 'sum' | 'perc' | 'mult'
 *  4. Arg		<- '(' Path ')'
 *  5. Path		<- [a-z]+ Filter? '.' Path | [a-z]+
 *  6. Filter	<- '[' [a-z]+ '=' ''' [a-z]+ ''' ',' Filter ']'| [a-z]+ '=' ''' [a-z]+ '''   
 * 
 */


public class GrammarTest {

	// Simple
	private String ruleSimple = "sum(sons.age)";

	// With filter
	private String ruleFilter = "sum(sons[sex='male'].age)";

	// With complex filter
	private String ruleComplexFilter = "sum(sons[sex='male' & nationality='ita'].age)";

	// With Projection
	private String ruleProjectionSimpleClass = "sum(sons.age) -> this.mySonsSummary.totalSonsAge";

	private String ruleProjectionSimpleField = "sum(sons.age) -> totalSonsAge";

}
